﻿using System;

namespace PopulationTask
{
    public static class Population
    {
        public static int GetYears(int initP, double p, int v, int curP)
        {
            if (initP <= 0 || v < 0 || curP <= 0 || curP < initP)
            {
                throw new ArgumentException("Wr0ng!");
            }

            if (p < 0 || p > 100)
            {
                throw new ArgumentOutOfRangeException(nameof(p));
            }

            int y = 0;
            double popul = initP;
            while (popul < curP)
            {
                y++;
                popul *= 1 + (p / 100);
                popul += v;
            }

            return y;
        }
    }
}
